// Copyright Chris Richardson <chris@bpi.cam.ac.uk> January 2019
// Licensed LGPL-3.0+

#include <emscripten/bind.h>
#include <emscripten/val.h>

#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <iostream>

using namespace Eigen;

class FElement {
public:
  FElement(){};
  ~FElement(){};

  emscripten::val get_points() {
    int nx = 25;
    int ny = 25;
    int c = 0;
    _points.resize(nx * ny, 3);

    for (int i = 0; i < nx; ++i)
      for (int j = 0; j < ny; ++j) {
        _points.row(c) << (double)(i) / (double)(nx - 1),
            (double)j / (double)(ny - 1), 0.0;
        ++c;
      }

    int n = _points.rows() * _points.cols();
    emscripten::val v(emscripten::typed_memory_view(n, _points.data()));
    return v;
  }

  emscripten::val get_cells() {
    int nx = 25;

    int ny = 25;
    int ntri = (nx - 1) * (ny - 1) * 2;
    _cells.resize(ntri, 4);

    int c = 0;
    for (int i = 0; i < nx - 1; ++i)
      for (int j = 0; j < ny - 1; ++j) {
        int ij = j + i * ny;
        _cells.row(c) << 3, ij, ij + ny, ij + ny + 1;
        _cells.row(c + 1) << 3, ij + 1, ij, ij + ny + 1;
        c += 2;
      }

    int n = _cells.rows() * _cells.cols();
    emscripten::val v(emscripten::typed_memory_view(n, _cells.data()));
    return v;
  }

  emscripten::val get_data() {

    emscripten::val v(
        emscripten::typed_memory_view(_data.size(), _data.data()));
    return v;
  }

  Eigen::SparseMatrix<double, ColMajor> assemble() {

    int m = _points.rows();
    SparseMatrix<double, ColMajor> A;
    A.resize(m, m);

    int n = _cells.rows();
    for (int i = 0; i < n; ++i) {
      Eigen::RowVectorXd x0 = _points.row(_cells(i, 1));
      Eigen::RowVectorXd x1 = _points.row(_cells(i, 2));
      Eigen::RowVectorXd x2 = _points.row(_cells(i, 3));
      double Ae = std::fabs((x0[0] - x1[0]) * (x2[1] - x1[1]) -
                            (x0[1] - x1[1]) * (x2[0] - x1[0]));
      Eigen::Matrix<double, 2, 3, Eigen::RowMajor> B;
      B << x1[1] - x2[1], x2[1] - x0[1], x0[1] - x1[1], x2[0] - x1[0],
          x0[0] - x2[0], x1[0] - x0[0];

      Eigen::Matrix3d K = B.transpose() * B / (4 * Ae);

      for (int j = 0; j < 3; ++j)
        for (int k = 0; k < 3; ++k) {
          int jx = _cells(i, j + 1);
          int kx = _cells(i, k + 1);
          A.coeffRef(jx, kx) += K(j, k);
        }
    }

    return A;
  }

  void calculate(int xpos) {

    int n = _points.rows();
    _data.resize(n);
    Eigen::Map<Eigen::VectorXd> x(_data.data(), n);

    VectorXd b(n);
    double dx = 0.01;

    auto A = assemble();

    // Set zero row to zero and insert 1 on diagonal
    A.prune([&n](int i, int j, float) { return i != 0 and i != (n - 1); });
    A.insert(0, 0) = 1.0;
    A.insert(n - 1, n - 1) = 1.0;

    for (int i = 0; i < n; ++i) {
      b[i] = 0.0;
    }
    b[xpos % n] = 1.0;

    SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int>> solver;
    // Compute the ordering permutation vector from the structural pattern of
    // A
    solver.analyzePattern(A);
    // Compute the numerical factorization
    solver.factorize(A);
    // Use the factors to solve the linear system
    x = solver.solve(b);
    std::cout << x.maxCoeff() << " - " << x.minCoeff() << "\n";
    x.array() -= x.minCoeff();
    x.array() /= x.maxCoeff();
    std::cout << x.maxCoeff() << " - " << x.minCoeff() << "\n";

    _points.col(2) = x;
  }

  std::vector<double> _data;

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
      _points;

  Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> _cells;
};

EMSCRIPTEN_BINDINGS(em_mod) {
  emscripten::class_<FElement>("FElement")
      .constructor<>()
      .function("calculate", &FElement::calculate)
      .function("get_points", &FElement::get_points)
      .function("get_cells", &FElement::get_cells)
      .function("get_data", &FElement::get_data);
}
