// Copyright 2011 The Emscripten Authors.  All rights reserved.
// Emscripten is available under two separate licenses, the MIT license and the
// University of Illinois/NCSA Open Source License.  Both these licenses can be
// found in the LICENSE file.

#include <emscripten/bind.h>

#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <iostream>

using namespace Eigen;

int solver() {

  int n = 30;

  VectorXd x(n), b(n);
  SparseMatrix<double, ColMajor> A(n, n);
  for (int i = 0; i < n; ++i) {
    b[i] = (double)i / (double)n;
    A.insert(i, i) = 2.0;
    if (i < n - 1)
      A.insert(i, i + 1) = -1.0;
    if (i > 0)
      A.insert(i, i - 1) = -1.0;
  }

  SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int>

           >
      solver;
  // fill A and b;
  // Compute the ordering permutation vector from the structural pattern of A
  solver.analyzePattern(A);
  // Compute the numerical factorization
  solver.factorize(A);
  // Use the factors to solve the linear system
  x = solver.solve(b);

  for (int i = 0; i < n; ++i)
    std::cout << x[i] << "\n";

  return 42;
}

EMSCRIPTEN_BINDINGS(fun_mod) { emscripten::function("solver", &solver); }
