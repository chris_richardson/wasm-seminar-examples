
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <iostream>

using namespace Eigen;

int main() {

  int n = 30;

  VectorXd x(n), b(n);
  SparseMatrix<double, ColMajor> A(n, n);
  for (int i = 0; i < n; ++i) {
    double x = (double)(i - n / 2) / n;
    b[i] = exp(-10 * x * x);
    A.insert(i, i) = 2.0;
    if (i < n - 1)
      A.insert(i, i + 1) = -1.0;
    if (i > 0)
      A.insert(i, i - 1) = -1.0;
  }

  SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> > solver;

  solver.analyzePattern(A);
  solver.factorize(A);
  x = solver.solve(b);

  Array<double, Dynamic, Dynamic> xx(x.size(), 2);
  xx.col(0) = b;
  xx.col(1) = x;

  std::cout << xx << "\n";

  return 0;
}
