#!/bin/bash
set -v
g++ -I/usr/include/eigen3 -std=c++11 hello.cpp
em++ -I/usr/include/eigen3 -std=c++11 hello.cpp -o hello.html
em++ -I/usr/include/eigen3 -std=c++11 --bind hello2.cpp -o hello2.js
